<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title inertia>{{ config('app.name', 'Test Skill PT. Max Solution Indonesia') }}</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma-rtl.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/bulma.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/css/additional.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/animations.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/custel.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::to('css/style.css') }}">

        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        @routes
        @vite(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"])
        @inertiaHead
    </head>
    <body class="font-sans antialiased">
        @inertia

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" type="text/css" href="{{ URL::to('bulma/js/navbar.js') }}">
    </body>
</html>
