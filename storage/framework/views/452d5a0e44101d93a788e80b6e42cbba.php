<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <title inertia><?php echo e(config('app.name', 'Test Skill PT. Max Solution Indonesia')); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/css/bulma-rtl.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/css/bulma-rtl.min.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/css/bulma.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/css/bulma.min.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/css/additional.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('css/animations.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('css/custel.css')); ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('css/style.css')); ?>">

        <script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
        <?php echo app('Tightenco\Ziggy\BladeRouteGenerator')->generate(); ?>
        <?php echo app('Illuminate\Foundation\Vite')(['resources/js/app.js', "resources/js/Pages/{$page['component']}.vue"]); ?>
        <?php if (!isset($__inertiaSsrDispatched)) { $__inertiaSsrDispatched = true; $__inertiaSsrResponse = app(\Inertia\Ssr\Gateway::class)->dispatch($page); }  if ($__inertiaSsrResponse) { echo $__inertiaSsrResponse->head; } ?>
    </head>
    <body class="font-sans antialiased">
        <?php if (!isset($__inertiaSsrDispatched)) { $__inertiaSsrDispatched = true; $__inertiaSsrResponse = app(\Inertia\Ssr\Gateway::class)->dispatch($page); }  if ($__inertiaSsrResponse) { echo $__inertiaSsrResponse->body; } else { ?><div id="app" data-page="<?php echo e(json_encode($page)); ?>"></div><?php } ?>

        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo e(URL::to('bulma/js/navbar.js')); ?>">
    </body>
</html>
<?php /**PATH C:\xampp\htdocs\testskill_pt_max_solution_indonesia\resources\views/app.blade.php ENDPATH**/ ?>