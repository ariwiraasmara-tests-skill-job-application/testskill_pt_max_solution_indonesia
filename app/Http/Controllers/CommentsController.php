<?php
namespace App\Http\Controllers;

use App\Models\Comments;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function get($id) {
        $user = @$_COOKIE['user'];
        if($user != null) {
            $data = Comments::where(['id'=>$id])->get();
            return response()->json(['msg'=>'You\'re Comment!', 'success'=>1, 'data'=>$data], 200);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {}

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $validated = $request->validate([
                'comment' => 'required|string'
            ]);
            $data = array('id_post'     => $request->id_post,
                          'by'          => $request->by,
                          'date'        => date('Y-m-d H:i:s'),
                          'comment'     => $request->comment);
            $res = Comments::create($data);
            if($res) {
                return response()->json(['msg'=>'You\'re Comment Created!', 'success'=>2, 'data'=>$data], 201);
            }
            return response()->json(['msg'=>'Fail To Create You\'re Comment!', 'error'=>1, 'data'=>null], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

    /**
     * Display the specified resource.
     */
    public function show(Comments $comments) {}

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Comments $comments) {}

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Comments $comments, $id) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $validated = $request->validate([
                'comment' => 'required|string'
            ]);

            $data = array('comment' => $request->comment);
            $res = Comments::where(['id'=>$id])->update($data);
            if($res) {
                return response()->json(['msg'=>'You\'re Comment has been Updated!', 'success'=>1, 'data'=>$data], 200);
            }
            return response()->json(['msg'=>'Fail To Update You\'re Comment!', 'error'=>2, 'data'=>$data], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Comments $comments, $id) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $res = Comments::where(['id'=>$id])->delete();
            if($res) {
                return response()->json(['msg'=>'You\'re Comment has been deleted!', 'success'=>1], 200);
            }
            return response()->json(['msg'=>'Fail To Delete You\'re Comment!', 'error'=>1], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }
}
