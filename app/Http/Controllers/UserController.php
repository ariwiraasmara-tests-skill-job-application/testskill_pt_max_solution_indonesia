<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Inertia\Inertia;
use Response;

class UserController extends Controller {
    //

    public function viewlogin() {
        $islogin = @$_COOKIE['islogin'];
        return Inertia::render('mlogin', ['islogin'=>$islogin]);
    }

    public function viewregister() {
        $islogin = @$_COOKIE['islogin'];
        return Inertia::render('mregister', ['islogin'=>$islogin]);
    }

    public function mlogin(Request $request, User $user) {
        $credentials = $request->validate([
            'email'     => ['required', 'email'],
            'password'  => ['required'],
        ]);

        if($user->where(['email'=>$request->email])->first()) {
            if($user->where(['password'=> Hash::make($request->password)])) {
                $credentials = $request->only('email', 'password');

                if (Auth::attempt($credentials)) {
                    // Authentication passed...
                    $data = $user->select('id', 'name', 'email', 'remember_token')->where(['email'=>$request->email])->first();

                    return response()->json(['msg'=>'Welcome!', 'success'=>1, 'data'=>$data, 'login_at'=>date('Y-m-d H:i:s')], 200)
                                     ->cookie('islogin', Hash::make(Crypt::encryptString(Hash::make(1))), 7*24*60)
                                     ->cookie('user', Crypt::encryptString($request->email), 7*24*60);
                }

                return response()->json(['msg'=>'The Authentication Not Passed!', 'success'=>-3, 'data'=>null], 400);
            }

            return response()->json(['msg'=>'Your Password is wrong! Try Again!', 'success'=>-2, 'data'=>null], 400);
        }

        return response()->json(['msg'=>'Your Password is wrong! Try Again!', 'success'=>-1, 'data'=>null], 400);
    }

    public function mregister(Request $request, User $user) {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',
            'password' => 'required',
        ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make($request->password),
        ];

        $res = $user->create($data);
        if($res) {
            return response()->json(['msg'=>'Congratulation! You had registered!', 'success'=>1, 'data'=>$data, 'register_at'=>date('Y-m-d H:i:s')], 201);
        }
        return response()->json(['msg'=>'Register Fail! Try Again!', 'success'=>-1, 'data'=>null], 400);
    }

    public function mlogout() {
        $user = @$_COOKIE['user'];
        if($user != null) {
            Auth::logout();
            return response()->json(['msg'=>'Goodbye!', 'success'=>1, 'data'=>null, 'logout_at'=>date('Y-m-d H:i:s')])
                            ->cookie('islogin', null)
                            ->cookie('user', null);
            }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

}
