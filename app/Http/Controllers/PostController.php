<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Post;
use App\Models\Comments;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Inertia\Inertia;
use Cookie;
use Route;

class PostController extends Controller {
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request) {
        //
        $home = route('home');
        $login = route('mlogin');
        $signin = route('msignin');
        $create = route('new_post');
        $islogin = @$_COOKIE['islogin'];
        return Inertia::render('post/dashboard',
                              ['homelink'   => $home,
                               'loginlink'  => $login,
                               'signinlink' => $signin,
                               'createlink' => $create,
                               'islogin'    => $islogin]);
    }

    public function getAll() {
        $post = Post::leftJoin('users', 'users.id', '=', 'posts.id')
                    ->select('posts.id', 'posts.title', 'users.name as author', 'posts.date')
                    ->orderBy('date', 'DESC')
                    ->paginate(3);
        return response()->json(['msg'=>'All Blog Post', 'success'=>1, 'data'=>$post], 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        //
        $islogin = @$_COOKIE['islogin'];
        $home = route('home');
        $create = route('new_post');
        if($islogin != null) {
            return Inertia::render('post/create_edit',
                                    ['homelink'  => $home,
                                    'createlink' => $create,
                                    'headtitle'  => 'New Post',
                                    'islogin'    => $islogin]);
        }
        return redirect()->route('home');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Post $post) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $validated = $request->validate([
                'title'   => 'required|string',
                'date'    => 'required|string',
                'content' => 'required|string'
            ]);

            $ds = User::where(['email' => Crypt::decryptString($user)])->first();
            // return $ds['id'];
            $data = array('title'    => $request->title,
                          'author'   => $ds['id'],
                          'date'     => $request->date,
                          'content'  => $request->content);
                        //   'created_date' => date('Y-m-d H:i:s'),
                        //   'updated_date' => null);
            $res = Post::create($data);
            if($res) {
                return response()->json(['msg'=>'Save Success!', 'success'=>2, 'data'=>$data], 201);
            }
            return response()->json(['msg'=>'Save Fail!', 'error'=>1, 'data'=>null], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

    /**
     * Display the specified resource.
     */
    public function show($id) {
        //
        // return 'post detail';
        $islogin = null;
        $id_user = 0;
        if(isset($_COOKIE['islogin'])) {
            $islogin = @$_COOKIE['islogin'];
            $email = Crypt::decryptString(@$_COOKIE['user']);
            $user = User::where(['email'=>$email])->get();
            $id_user = $user[0]['id'];
        }
        $home = route('home');
        $login = route('mlogin');
        $signin = route('msignin');
        $create = route('new_post');

        return Inertia::render('post/detail',
                                ['homelink'  => $home,
                                'loginlink'  => $login,
                                'signinlink' => $signin,
                                'createlink' => $create,
                                'islogin'    => $islogin,
                                'id'         => $id,
                                'user'       => $id_user,
                                'islogin'    => $islogin]);
    }

    public function get(Post $post, $id) {
        $user = @$_COOKIE['user'];
        if($user != null) {
            $post = Post::select('posts.id', 'posts.author as id_user', 'posts.title', 'users.name as author', 'posts.date', 'posts.content')
                        ->join('users', 'users.id', '=', 'posts.id')
                        ->where(['posts.id'=>$id])
                        ->get();
            $comments = Comments::select('comments.id', 'comments.id_post', 'comments.by as id_user', 'users.name as by', 'comments.date', 'comments.comment')
                                ->join('users', 'users.id', '=', 'comments.by')
                                ->where(['comments.id_post'=>$id])
                                ->get();

            return response()->json(['msg'=>'Post Detail!', 'success'=>1, 'data'=>['post'=>$post,'comments'=>$comments]], 200);
        }

        $post = Post::select('posts.id', 'posts.title', 'users.name as author', 'posts.date', 'posts.content')
                    ->join('users', 'users.id', '=', 'posts.id')
                    ->where(['posts.id'=>$id])
                    ->get();

        $comments = Comments::select('comments.id', 'comments.id_post', 'users.name as by', 'comments.date', 'comments.comment')
                            ->join('users', 'users.id', '=', 'comments.by')
                            ->where(['comments.id_post'=>$id])
                            ->get();

        return response()->json(['msg'=>'Post Detail!', 'success'=>1, 'data'=>['post'=>$post,'comments'=>$comments]], 200);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Post $post, $id) {
        //
        $islogin = @$_COOKIE['islogin'];
        $home = route('home');
        $create = route('new_post');
        if($islogin != null) {
            $data = Post::where(['id'=>$id])->get();
            return Inertia::render('post/create_edit',
                                    ['homelink'   => $home,
                                     'createlink' => $create,
                                     'islogin'    => $islogin,
                                     'headtitle'  => 'Edit Post',
                                     'data'       => $data]);
        }
        return redirect()->route('home');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Post $post, $id) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $validated = $request->validate([
                'title'   => 'required|string',
                'date'    => 'required|string',
                'content' => 'required|string'
            ]);

            $data = array('title'    => $request->title,
                          'date'     => $request->date,
                          'content'  => $request->content);
                        //   'created_date' => null,
                        //   'updated_date' => date('Y-m-d H:i:s'));
            $res = Post::where(['id'=>$id])->update($data);
            if($res) {
                return response()->json(['msg'=>'Save Success!', 'success'=>1, 'data'=>$data], 200);
            }
            return response()->json(['msg'=>'Update Fail!', 'error'=>2, 'data'=>$data], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Post $post, $id) {
        //
        $user = @$_COOKIE['user'];
        if($user != null) {
            $res = Post::where(['id'=>$id])->delete();
            if($res) {
                return response()->json(['msg'=>'You\'re Post has been deleted!', 'success'=>1], 200);
            }
            return response()->json(['msg'=>'Fail To Delete You\'re Post!', 'error'=>1], 400);
        }
        return response()->json(['msg'=>'You\'re Unauthorized!', 'error'=>1, 'data'=>null], 400);
    }
}
