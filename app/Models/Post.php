<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable = ["title",
                            "author",
                            "date",
                            "content"];

    public $timestamps = false;
    // const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'updated_date';

}
