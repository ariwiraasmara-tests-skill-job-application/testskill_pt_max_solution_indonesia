<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model {
    use HasFactory;

    protected $guarded = [];
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = ["id_post",
                            "by",
                            "date",
                            "comment"];

    public $timestamps = false;
    // const CREATED_AT = 'creation_date';
    // const UPDATED_AT = 'updated_date';

}
