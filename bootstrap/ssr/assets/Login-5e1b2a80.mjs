import { resolveComponent, unref, useSSRContext } from "vue";
import { ssrRenderComponent, ssrInterpolate, ssrRenderAttr, ssrIncludeBooleanAttr, ssrLooseContain } from "vue/server-renderer";
import { useForm, Head } from "@inertiajs/vue3";
const Login_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  __name: "Login",
  __ssrInlineRender: true,
  props: {
    canResetPassword: {
      type: Boolean
    },
    status: {
      type: String
    }
  },
  setup(__props) {
    const form = useForm({
      email: "",
      password: "",
      remember: false
    });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_InputError = resolveComponent("InputError");
      _push(`<!--[-->`);
      _push(ssrRenderComponent(unref(Head), { title: "Log in" }, null, _parent));
      if (__props.status) {
        _push(`<div class="mb-4 font-medium text-sm text-green-600">${ssrInterpolate(__props.status)}</div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`<form class="p-30 form"><div class="control"><input type="email" name="email" id="email" class="input" placeholder="Email.."${ssrRenderAttr("value", unref(form).email)} required>`);
      _push(ssrRenderComponent(_component_InputError, {
        class: "m-t-5",
        message: unref(form).errors.email
      }, null, _parent));
      _push(`</div><div class="control m-t-10"><input type="password" name="password" id="password" class="input" placeholder="Password.."${ssrRenderAttr("value", unref(form).password)} required>`);
      _push(ssrRenderComponent(_component_InputError, {
        class: "m-t-5",
        message: unref(form).errors.password
      }, null, _parent));
      _push(`</div><div class="block mt-4"><label class="checkbox"><input type="checkbox" name="remember" id="remember"${ssrIncludeBooleanAttr(Array.isArray(unref(form).remember) ? ssrLooseContain(unref(form).remember, null) : unref(form).remember) ? " checked" : ""}> Remember me </label></div><div class="m-t-30"><div class="buttons has-addons is-centered"><button type="submit" name="isok" id="isok" title="OK" class="button is-success is-link is-rounded is-medium"> Login </button><a href="register" title="Sign In" class="button is-link is-rounded is-medium"> Sign In </a></div></div></form><!--]-->`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/Login.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
