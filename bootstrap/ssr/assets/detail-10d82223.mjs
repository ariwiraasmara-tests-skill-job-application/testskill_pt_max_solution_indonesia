import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { N as Navbar } from "./NavBar-2eb6329b.mjs";
import Cookies from "js-cookie";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrInterpolate, ssrRenderAttr, ssrRenderList } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = {
  name: "detailpost",
  components: {
    Head,
    Navbar,
    Cookies
  },
  props: {
    homelink: {
      type: String,
      default: null
    },
    signinlink: {
      type: String,
      default: null
    },
    loginlink: {
      type: String,
      default: null
    },
    createlink: {
      type: String,
      default: null
    },
    id: {
      type: String,
      default: null
    },
    user: {
      type: String,
      default: null
    },
    islogin: {
      type: String,
      default: false
    }
  },
  data() {
    return {
      post: null,
      comments: null
    };
  },
  created() {
    this.getPost();
  },
  methods: {
    getPost() {
      axios.get("../api/post/" + this.id).then((response) => {
        this.post = response.data.data.post[0];
        this.comments = response.data.data.comments;
      }).catch((error) => {
        console.log("error", error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Navbar = resolveComponent("Navbar");
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, {
    title: $data.post.title
  }, null, _parent));
  _push(ssrRenderComponent(_component_Navbar, {
    homelink: $props.homelink,
    loginlink: $props.loginlink,
    signinlink: $props.signinlink,
    createlink: $props.createlink,
    islogin: $props.islogin
  }, null, _parent));
  _push(`<div class="p-30 is-desktop is-mobile text-is-black"><h1 class="title bold text-is-black">${ssrInterpolate($data.post.title)}</h1><div class="m-t-10"><span class="bold">Author : ${ssrInterpolate($data.post.author)}</span> | <span class="italic">${ssrInterpolate($data.post.date)}</span>`);
  if ($props.islogin != null) {
    _push(`<span>, <a${ssrRenderAttr("href", "../post/edit/" + $data.post.id)}>`);
    _push(ssrRenderComponent(_component_ion_icon, { name: "create-outline" }, null, _parent));
    _push(` Edit </a></span>`);
  } else {
    _push(`<!---->`);
  }
  _push(`</div><div class="m-t-30 justify">${ssrInterpolate($data.post.content)}</div><div class="block-comments">`);
  if ($props.islogin != null) {
    _push(`<span><a href="/comment/create" class="is-link">`);
    _push(ssrRenderComponent(_component_ion_icon, { name: "create-outline" }, null, _parent));
    _push(` Add Comment </a> | </span>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<span class="bold text-is-black underline">Comments</span> : <!--[-->`);
  ssrRenderList($data.comments, (cm, x) => {
    _push(`<div class="comment"><span class="bold">By: ${ssrInterpolate(cm.by)}</span> | <span class="italic">${ssrInterpolate(cm.date)}</span><br><p class="m-t-10">${ssrInterpolate(cm.comment)}</p>`);
    if ($props.islogin != null && $props.user == $data.post.id_user) {
      _push(`<p class="m-t-10"><a${ssrRenderAttr("href", "/comment/edit/" + cm.id)} class="underline">Edit</a> | <a${ssrRenderAttr("href", "/comment/delete/" + cm.id)} class="underline">Delete</a></p>`);
    } else {
      _push(`<!---->`);
    }
    _push(`</div>`);
  });
  _push(`<!--]--></div></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/post/detail.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const detail = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  detail as default
};
