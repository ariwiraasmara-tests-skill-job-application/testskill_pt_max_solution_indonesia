import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { N as Navbar } from "./NavBar-2eb6329b.mjs";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrRenderList, ssrRenderAttr, ssrInterpolate } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = {
  name: "dashboard",
  props: {
    homelink: {
      type: String,
      default: null
    },
    signinlink: {
      type: String,
      default: null
    },
    loginlink: {
      type: String,
      default: null
    },
    createlink: {
      type: String,
      default: null
    },
    islogin: {
      type: String,
      default: false
    }
  },
  components: {
    Head,
    Navbar
  },
  data() {
    return {
      title: "Welcome",
      list_post: null
    };
  },
  created() {
    this.getAll(0);
  },
  methods: {
    getAll(page) {
      axios.get("../api/post/all?page=" + page).then((response) => {
        this.list_post = response.data.data.data;
      }).catch((error) => {
        console.log("error", error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_Navbar = resolveComponent("Navbar");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: $data.title }, null, _parent));
  _push(ssrRenderComponent(_component_Navbar, {
    homelink: $props.homelink,
    loginlink: $props.loginlink,
    signinlink: $props.signinlink,
    createlink: $props.createlink,
    islogin: $props.islogin
  }, null, _parent));
  _push(`<div class="p-30 is-desktop is-mobile text-is-black"><!--[-->`);
  ssrRenderList($data.list_post, (p, x) => {
    _push(`<div><div class="post"><div class="title bold text-is-black"><a${ssrRenderAttr("href", "post/" + p.id)}>${ssrInterpolate(p.title)}</a></div><div class="m-t-10"><span class="bold">${ssrInterpolate(p.date)}</span> | <span>${ssrInterpolate(p.author)}</span></div></div></div>`);
  });
  _push(`<!--]--></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/post/dashboard.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const dashboard = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  dashboard as default
};
