import axios from "axios";
import Swal from "sweetalert2";
import { Head } from "@inertiajs/vue3";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrRenderAttr } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const mregister_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "register",
  components: {
    Head
  },
  props: {
    islogin: {
      type: String,
      default: null
    }
  },
  data() {
    return {
      name: null,
      email: null,
      password: null,
      password_confirmation: null,
      islogin: null
    };
  },
  created() {
    if (this.islogin != null)
      window.location.href = "/dashboard";
  },
  methods: {
    register() {
      axios.post("../api/mregister", {
        name: this.name,
        email: this.email,
        password: this.password
      }).then((response) => {
        Swal.fire({
          title: "Register Success!",
          icon: "success",
          confirmButtonText: "OK"
        }).then((result) => {
          window.location.href = "/login";
        });
      }).catch((error) => {
        console.log("error", error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputError = resolveComponent("InputError");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Register" }, null, _parent));
  _push(`<form class="p-30 form"><div class="control"><input type="text" name="name" id="name" class="input" placeholder="Name.."${ssrRenderAttr("value", $data.name)} required autofocus autocomplete="name">`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: "Name is required"
  }, null, _parent));
  _push(`</div><div class="control m-t-10"><input type="email" name="email" id="email" class="input" placeholder="Email.."${ssrRenderAttr("value", $data.email)} required autofocus autocomplete="username">`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: "Email is required"
  }, null, _parent));
  _push(`</div><div class="control m-t-10"><input type="password" name="password" id="password" class="input" placeholder="Password.."${ssrRenderAttr("value", $data.password)} required autofocus autocomplete="new-password">`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: "Password is required"
  }, null, _parent));
  _push(`</div><div class="control m-t-10"><input type="password" name="password_confirmation" id="password_confirmation" class="input" placeholder="Confirm Password.."${ssrRenderAttr("value", $data.password_confirmation)} required autofocus autocomplete="new-password">`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "mt-2",
    message: "Confirm Password is required"
  }, null, _parent));
  _push(`</div><div class="m-t-30"><div class="buttons has-addons is-centered"><button type="submit" name="isok" id="isok" title="OK" class="button is-success is-link is-rounded is-medium"> Register </button><a href="login" title="Sign In" class="button is-link is-rounded is-medium"> Already registered? </a></div></div></form><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/mregister.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const mregister = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  mregister as default
};
