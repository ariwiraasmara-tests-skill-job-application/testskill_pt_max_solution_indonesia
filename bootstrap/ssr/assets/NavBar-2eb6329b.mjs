import axios from "axios";
import { resolveComponent, mergeProps, useSSRContext } from "vue";
import { ssrRenderAttrs, ssrRenderAttr, ssrRenderComponent } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = {
  name: "navbar",
  props: {
    homelink: {
      type: String,
      default: null
    },
    signinlink: {
      type: String,
      default: null
    },
    loginlink: {
      type: String,
      default: null
    },
    createlink: {
      type: String,
      default: null
    },
    islogin: {
      type: String,
      default: false
    }
  },
  data() {
    return {};
  },
  methods: {
    toLogout() {
      axios.get("../api/mlogout").then((response) => {
        window.location.href = "/";
      }).catch((error) => {
        console.log("error", error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_ion_icon = resolveComponent("ion-icon");
  _push(`<nav${ssrRenderAttrs(mergeProps({
    class: "level",
    role: "navigation",
    "aria-label": "main navigation"
  }, _attrs))}>`);
  if ($props.islogin == null) {
    _push(`<p class="level-item has-text-centered"><a${ssrRenderAttr("href", $props.loginlink)} class="bold underline is-link"> LogIn </a></p>`);
  } else {
    _push(`<!---->`);
  }
  if ($props.islogin != null) {
    _push(`<p class="level-item has-text-centered"><a href="#logout" class="bold underline is-link"> Logout </a></p>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<p class="level-item has-text-centered"><a${ssrRenderAttr("href", $props.homelink)} class="title">`);
  _push(ssrRenderComponent(_component_ion_icon, { name: "home-outline" }, null, _parent));
  _push(`</a></p>`);
  if ($props.islogin == null) {
    _push(`<p class="level-item has-text-centered"><a${ssrRenderAttr("href", $props.signinlink)} class="bold underline is-link"> Sign In </a></p>`);
  } else {
    _push(`<!---->`);
  }
  if ($props.islogin != null) {
    _push(`<p class="level-item has-text-centered"><a${ssrRenderAttr("href", $props.createlink)} class="bold underline is-link"> Create </a></p>`);
  } else {
    _push(`<!---->`);
  }
  _push(`</nav>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Components/NavBar.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const Navbar = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  Navbar as N
};
