import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrInterpolate, ssrRenderAttr } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const mlogin_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  name: "login",
  components: {
    Head
  },
  props: {
    islogin: {
      type: String,
      default: null
    }
  },
  data() {
    return {
      email: null,
      password: null,
      islogin: null
    };
  },
  created() {
    if (this.islogin != null)
      window.location.href = "/dashboard";
  },
  methods: {
    login() {
      axios.post("../api/mlogin", {
        email: this.email,
        password: this.password
      }).then((response) => {
        window.location.href = "dashboard";
      }).catch((error) => {
        console.log("error", error);
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputError = resolveComponent("InputError");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: "Log in" }, null, _parent));
  if (_ctx.status) {
    _push(`<div class="mb-4 font-medium text-sm text-green-600">${ssrInterpolate(_ctx.status)}</div>`);
  } else {
    _push(`<!---->`);
  }
  _push(`<form class="p-30 form"><div class="control"><input type="email" name="email" id="email" class="input" placeholder="Email.."${ssrRenderAttr("value", $data.email)} required>`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "m-t-5",
    message: "Email is required!"
  }, null, _parent));
  _push(`</div><div class="control m-t-10"><input type="password" name="password" id="password" class="input" placeholder="Password.."${ssrRenderAttr("value", $data.password)} required>`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "m-t-5",
    message: "Password is required!"
  }, null, _parent));
  _push(`</div><div class="m-t-30"><div class="buttons has-addons is-centered"><button type="submit" name="isok" id="isok" title="OK" class="button is-success is-link is-rounded is-medium"> Login </button><a href="msignin" title="Sign In" class="button is-link is-rounded is-medium"> Sign In </a></div></div></form><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/mlogin.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const mlogin = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  mlogin as default
};
