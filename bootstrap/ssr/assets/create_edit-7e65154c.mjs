import axios from "axios";
import { Head } from "@inertiajs/vue3";
import { N as Navbar } from "./NavBar-2eb6329b.mjs";
import { resolveComponent, useSSRContext } from "vue";
import { ssrRenderComponent, ssrInterpolate, ssrRenderAttr } from "vue/server-renderer";
import { _ as _export_sfc } from "./_plugin-vue_export-helper-cc2b3d55.mjs";
const _sfc_main = {
  name: "create-edit",
  components: {
    Head,
    Navbar
  },
  props: {
    homelink: {
      type: String,
      default: null
    },
    signinlink: {
      type: String,
      default: null
    },
    loginlink: {
      type: String,
      default: null
    },
    createlink: {
      type: String,
      default: null
    },
    islogin: {
      type: String,
      default: false
    },
    headtitle: {
      type: String,
      default: false
    },
    data: {
      type: Object,
      default: null
    }
  },
  data() {
    return {
      title: null,
      date: null,
      content: null
    };
  },
  created() {
    if (this.data != null)
      this.getPost();
  },
  methods: {
    getPost() {
      this.title = data.title;
      this.date = data.date;
      this.content = data.content;
    },
    savePost() {
      if (this.data != null) {
        axios.post("../api/post/update/" + this.data.id, {
          title: this.title,
          date: this.date,
          content: this.content
        }).then((response) => {
          Swal.fire({
            title: "Success Update Post!",
            icon: "success",
            confirmButtonText: "OK"
          }).then((result) => {
            window.location.href = "/post/get/" + this.data.id;
          });
        }).catch((error) => {
          console.log("error", error);
        });
      } else {
        axios.post("../api/post/save/", {
          title: this.title,
          date: this.date,
          content: this.content
        }).then((response) => {
          Swal.fire({
            title: "Success Create New Post!",
            icon: "success",
            confirmButtonText: "OK"
          }).then((result) => {
            window.location.href = "/dashboard";
          });
        }).catch((error) => {
          console.log("error", error);
        });
      }
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Head = resolveComponent("Head");
  const _component_InputError = resolveComponent("InputError");
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Head, { title: $props.headtitle }, null, _parent));
  _push(`<div class="p-30"><h1 class="title bold text-is-black center">${ssrInterpolate($props.headtitle)}</h1><form class="m-t-30"><div class="columns"><div class="control column is-6"><input type="text" name="title" id="title" class="input" placeholder="Title.."${ssrRenderAttr("value", $data.title)} required>`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "m-t-5",
    message: "Title is required!"
  }, null, _parent));
  _push(`</div><div class="control column is-6"><input type="date" name="date" id="date" class="input" placeholder="Date.."${ssrRenderAttr("value", $data.date)} required>`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "m-t-5",
    message: "Date is required!"
  }, null, _parent));
  _push(`</div><div class="control column is-12"><textarea name="content" id="content" class="textarea" placeholder="Content.." required>${ssrInterpolate($data.content)}</textarea>`);
  _push(ssrRenderComponent(_component_InputError, {
    class: "m-t-5",
    message: "Content is required!"
  }, null, _parent));
  _push(`</div></div></form></div><!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/post/create_edit.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const create_edit = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);
export {
  create_edit as default
};
