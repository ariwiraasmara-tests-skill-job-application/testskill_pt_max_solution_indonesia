import { unref, useSSRContext } from "vue";
import { ssrRenderComponent, ssrRenderAttr } from "vue/server-renderer";
import "./GuestLayout-af673049.mjs";
import { b as _sfc_main$1 } from "./TextInput-27676015.mjs";
import "./PrimaryButton-272e1e06.mjs";
import { useForm, Head } from "@inertiajs/vue3";
import "./ApplicationLogo-8b847249.mjs";
import "./_plugin-vue_export-helper-cc2b3d55.mjs";
const Register_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  __name: "Register",
  __ssrInlineRender: true,
  setup(__props) {
    const form = useForm({
      name: "",
      email: "",
      password: "",
      password_confirmation: ""
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<!--[-->`);
      _push(ssrRenderComponent(unref(Head), { title: "Register" }, null, _parent));
      _push(`<form class="p-30 form"><div class="control"><input type="text" name="name" id="name" class="input" placeholder="Name.."${ssrRenderAttr("value", unref(form).name)} required autofocus autocomplete="name">`);
      _push(ssrRenderComponent(_sfc_main$1, {
        class: "mt-2",
        message: unref(form).errors.name
      }, null, _parent));
      _push(`</div><div class="control m-t-10"><input type="email" name="email" id="email" class="input" placeholder="Email.."${ssrRenderAttr("value", unref(form).email)} required autofocus autocomplete="username">`);
      _push(ssrRenderComponent(_sfc_main$1, {
        class: "mt-2",
        message: unref(form).errors.email
      }, null, _parent));
      _push(`</div><div class="control m-t-10"><input type="password" name="password" id="password" class="input" placeholder="Password.."${ssrRenderAttr("value", unref(form).password)} required autofocus autocomplete="new-password">`);
      _push(ssrRenderComponent(_sfc_main$1, {
        class: "mt-2",
        message: unref(form).errors.password
      }, null, _parent));
      _push(`</div><div class="control m-t-10"><input type="password" name="password_confirmation" id="password_confirmation" class="input" placeholder="Confirm Password.."${ssrRenderAttr("value", unref(form).password_confirmation)} required autofocus autocomplete="new-password">`);
      _push(ssrRenderComponent(_sfc_main$1, {
        class: "mt-2",
        message: unref(form).errors.password_confirmation
      }, null, _parent));
      _push(`</div><div class="m-t-30"><div class="buttons has-addons is-centered"><button type="submit" name="isok" id="isok" title="OK" class="button is-success is-link is-rounded is-medium"> Register </button><a href="login" title="Sign In" class="button is-link is-rounded is-medium"> Already registered? </a></div></div></form><!--]-->`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("resources/js/Pages/Auth/Register.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
export {
  _sfc_main as default
};
