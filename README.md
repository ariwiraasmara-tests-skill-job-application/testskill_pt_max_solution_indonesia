# testskill_pt_max_solution_indonesia

# About
Test Skill PT Max Solution Indonesia by Syahri Ramadhan Wiraasmara (ARI)

# Powered by
Gitlab,
https://gitlab.com

Composer,
https://getcomposer.org/

Laravel,
https://laravel.com/

VueJS,
https://vuejs.org/

BulmaCSS,
https://bulma.io/

# HOW TO USE
1. Open 1 terminal then execute 'npm run dev' to run nodejs
2. Open 1 terminal again then execure 'php artisan serve' to run the server
3. Make sure MySQL database is turn on in XAMPP
