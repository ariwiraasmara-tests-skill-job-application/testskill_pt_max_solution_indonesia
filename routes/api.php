<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/mlogin', '\App\Http\Controllers\UserController@mlogin');
Route::get('/mlogout', '\App\Http\Controllers\UserController@mlogout');
Route::post('/mregister', '\App\Http\Controllers\UserController@mregister');

Route::get('/post/all', '\App\Http\Controllers\PostController@getAll');
Route::get('/post/{id}', '\App\Http\Controllers\PostController@get');
Route::post('/post/save', '\App\Http\Controllers\PostController@store');
Route::post('/post/update/{id}', '\App\Http\Controllers\PostController@update');
Route::post('/post/delete/{id}', '\App\Http\Controllers\PostController@delete');

Route::get('/comments/{id}', '\App\Http\Controllers\CommentsController@get');
Route::post('/comments/save', '\App\Http\Controllers\CommentsController@store');
Route::post('/comments/update/{id}', '\App\Http\Controllers\CommentsController@update');
Route::post('/comments/delete/{id}', '\App\Http\Controllers\CommentsController@destroy');

