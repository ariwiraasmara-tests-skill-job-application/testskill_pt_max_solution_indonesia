<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
*/

Route::get('/mlogin', '\App\Http\Controllers\UserController@viewlogin')->name('mlogin');

Route::get('/msignin', '\App\Http\Controllers\UserController@viewregister')->name('msignin');

Route::get('/post/create/', '\App\Http\Controllers\PostController@create')->name('new_post');
Route::get('/post/edit/{id}', '\App\Http\Controllers\PostController@edit')->name('edit_post');

Route::get('/', '\App\Http\Controllers\PostController@index')->name('home');
Route::get('/dashboard', '\App\Http\Controllers\PostController@index')->name('dashboard');
Route::get('/post', '\App\Http\Controllers\PostController@index')->name('dashboard');
Route::get('/post/{id}', '\App\Http\Controllers\PostController@show')->name('post_detail');

require __DIR__.'/auth.php';
