<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Post;
use App\Models\Comment;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(3)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        for($x = 1; $x < 4; $x++) {
            \App\Models\User::factory()->create([
                'name'              => 'Test User '.$x,
                'email'             => 'testusermail'.$x.'@example.com',
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password'          => Hash::make('password'),
                'remember_token'    => 'A1B2C3D4E5',
                // 'create_at'         => date('Y-m-d H:i:s'),
                // 'update_at'         => null
            ]);
        }

        for($x = 1; $x < 4; $x++) {
            \App\Models\Post::factory()->create([
                'title'     => 'Post Title '.$xx,
                'author'    => $y,
                'date'      => date('Y-m-d H:i:s'),
                'content'   => 'Post with some content on it and content on '.$y
            ]);
        }

        for($x=1; $x < 10; $x++) {
            $xx = $x;
            for($y = 1; $y < 4; $y++) {
                \App\Models\Comment::factory()->create([
                    'id_post'   => $x,
                    'date'      => date('Y-m-d H:i:s'),
                    'comment'   => 'Post with some content on it and content on '.$y
                ]);
            }
        }

    }
}
